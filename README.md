# ePRaSE App
The ePrescribing Risk and Safety Evaluation tool (ePRaSE) is designed to evaluate ePrescription services, in order to determine their usefulness and to encourage the correct use of these systems and deliver improved patient outcomes.

## Requirements
The app uses the following open Source Libraries...

To install, use the following commands:


* Run `npm install grunt`
* Run `npm install -g grunt-cli`




* Run `npm install` to install all dependencies?



* Run `npm install superlogin`
* Run `npm install angular@1.7.2`




To test
* Navigate to `eprase-app` folder
* Run `ng serve`
* Navigate to `http://localhost:4200/`