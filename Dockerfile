FROM node:8

COPY /eprase-app/ /usr/local/app/
COPY package*.json /usr/local/app/

WORKDIR /usr/local/app

RUN npm install

CMD ng serve