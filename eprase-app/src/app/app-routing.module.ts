import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from "./welcome/welcome.component";
import { AboutComponent } from "./about/about.component";
import { HomeScreenComponent } from "./home-screen/home-screen.component";
import {LoginComponent} from "./login/login.component";
import {ResultsHomeComponent} from "./results-home/results-home.component";
import {AssessmentIntroComponent} from "./assessment-intro/assessment-intro.component";
import {AssessmentResultsComponent} from "./assessment-results/assessment-results.component";
import {AssessmentHomeComponent} from "./assessment-home/assessment-home.component";
import {AssessmentInstructionsComponent} from "./assessment-instructions/assessment-instructions.component";
import {AssessmentPart1Component} from "./assessment-part1/assessment-part1.component";
import {AssessmentPart2Component} from "./assessment-part2/assessment-part2.component";
import {RegisterComponent} from "./register/register.component";

const routes: Routes = [
  { path: '', redirectTo: '/welcome', pathMatch: 'full' },
  { path: "about", component: AboutComponent },
  { path: "welcome", component: WelcomeComponent },
  { path: "home", component: HomeScreenComponent },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "assessment-intro", component: AssessmentIntroComponent },
  { path: "assessment-home", component: AssessmentHomeComponent },
  { path: "instructions", component: AssessmentInstructionsComponent },
  { path: "assessment-part1", component: AssessmentPart1Component },
  { path: "assessment-part2", component: AssessmentPart2Component },
  { path: "results-home", component: ResultsHomeComponent },
  { path: "assessment-results", component: AssessmentResultsComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }


