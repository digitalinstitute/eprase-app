import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssessmentIntroComponent } from './assessment-intro.component';

describe('AssessmentIntroComponent', () => {
  let component: AssessmentIntroComponent;
  let fixture: ComponentFixture<AssessmentIntroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssessmentIntroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssessmentIntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
