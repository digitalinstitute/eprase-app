import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-assessment-home',
  templateUrl: './assessment-intro.component.html',
  styleUrls: ['./assessment-intro.component.css']
})
export class AssessmentIntroComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onBackClick() : void {
    this.router.navigateByUrl('/home');
  }

  onStartAssessmentClick() : void {
    this.router.navigateByUrl('/assessment-home');
  }
}
