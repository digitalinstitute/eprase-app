import { Component, OnInit } from '@angular/core';
import { Institution, INSTITUTIONS } from "../institutions";
import { User } from "../user";
import { Router } from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  institutions: Institution[] = [];
  new_user: User;

  constructor(private router: Router) { }

  ngOnInit() {
    this.loadInstitutions();
    this.new_user=new User();
  }

  loadInstitutions() : void {
    this.institutions = INSTITUTIONS;

    this.institutions.sort(function(a, b){
      if(a.org_name < b.org_name) return -1;
      if(a.org_name > b.org_name) return 1;
      return 0;
    })
  }

  onRegisterClick() : void {
    console.log("REGISTER!!");

    this.router.navigateByUrl('/home');
  }
}
