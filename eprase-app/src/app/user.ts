export class User {
  id: number;
  username: string;
  institution: string;
}
