export class Patient {
  id: number;
  first_name: string;
  surname: string;
  dob: string;
  gender: string;
  problems: string;
  allergies: string;
}

export const PATIENTS: Patient[] = [
  { id: 1,   first_name: 'David',    surname: 'zzzSteel',    dob: '14/04/1985',   gender: 'male',    problems: 'Type 1 Diabetes',   allergies: 'None',      },
  { id: 2,   first_name: 'Thomas',   surname: 'zzzAshton',   dob: '04/09/1987',   gender: 'male',    problems: 'None',              allergies: 'NKA',       },
  { id: 3,   first_name: 'Alex',     surname: 'zzzKerrigan', dob: '27/03/1985',   gender: 'female',  problems: 'Type 2 Diabetes',   allergies: 'None',      },
  { id: 4,   first_name: 'Malcolm',  surname: 'zzzReynolds', dob: '24/04/1981',   gender: 'male',    problems: 'None',              allergies: 'None',      },
  { id: 5,   first_name: 'Kaylee',   surname: 'zzzFrye',     dob: '26/06/1995',   gender: 'female',  problems: 'None',              allergies: 'Penicillin',},
  { id: 6,   first_name: 'Lee',      surname: 'zzzThompkins',dob: '24/02/1965',   gender: 'female',  problems: 'Epilepsy',          allergies: 'None',      },
  { id: 7,   first_name: 'Jake',     surname: 'zzzSisko',    dob: '23/04/1975',   gender: 'male',    problems: 'None',              allergies: 'NKA',       },
  { id: 8,   first_name: 'Sun-hi',   surname: 'zzzKim',      dob: '13/08/1985',   gender: 'female',  problems: 'None',              allergies: 'None',      },
  { id: 9,   first_name: 'Sarah',    surname: 'zzzSteel',    dob: '09/01/1985',   gender: 'female',  problems: 'None',              allergies: 'Penicillin',},
  { id: 10,  first_name: 'Samuel',   surname: 'zzzPage',     dob: '04/04/1985',   gender: 'male',    problems: 'None',              allergies: 'None',      },
  { id: 11,  first_name: 'Gertrude', surname: 'zzzAdult',    dob: '22/05/1948',   gender: 'female',  problems: 'None',              allergies: 'None',      },
  { id: 12,  first_name: 'Dai',      surname: 'zzzLysis',    dob: '22/05/1963',   gender: 'male',    problems: 'None',              allergies: 'None',      },
  { id: 13,  first_name: 'Offah',    surname: 'zzzCockroft', dob: '22/05/1983',   gender: 'male',    problems: 'None',              allergies: 'None',      },
  { id: 14,  first_name: 'Cyril',    surname: "zzzO'sys",    dob: '22/05/1964',   gender: 'male',    problems: 'None',              allergies: 'None',      },
  { id: 15,  first_name: 'Kenny',    surname: 'zzzLight',    dob: '22/05/1981',   gender: 'male',    problems: 'None',              allergies: 'None',      },
  { id: 16,  first_name: 'Ann',      surname: 'zzzAemia',    dob: '22/05/1971',   gender: 'female',  problems: 'None',              allergies: 'None',      },
  { id: 17,  first_name: 'Una',      surname: 'zzzStress',   dob: '22/05/1993',   gender: 'female',  problems: 'None',              allergies: 'None',      },
  { id: 18,  first_name: 'Jim',      surname: 'zzzStatin',   dob: '22/05/1952',   gender: 'male',    problems: 'None',              allergies: 'NKA',       },
];
