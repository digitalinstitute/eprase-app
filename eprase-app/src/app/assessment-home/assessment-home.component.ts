import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Assessment, assessment} from "../assessment";

@Component({
  selector: 'app-assessment-home',
  templateUrl: './assessment-home.component.html',
  styleUrls: ['./assessment-home.component.css']
})
export class AssessmentHomeComponent implements OnInit {

  constructor(private router: Router, protected assessment: Assessment) { }

  ngOnInit() {
    if (!this.assessment.isIntitialised())
    {
      console.log("INIT");
      this.assessment.init();
    }
  }

  onBackClick() : void {
    this.router.navigateByUrl('/home');
  }

  onPart1Click() : void {
    this.router.navigateByUrl('/assessment-part1');
  }

  onHomeClick() : void {
    this.router.navigateByUrl('/home');
  }

  onResultsClick() : void {
    this.router.navigateByUrl('/results-home');
  }
}
