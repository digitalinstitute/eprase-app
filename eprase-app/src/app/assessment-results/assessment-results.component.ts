import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-assessment-results',
  templateUrl: './assessment-results.component.html',
  styleUrls: ['./assessment-results.component.css']
})
export class AssessmentResultsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onBackClick() : void {
    this.router.navigateByUrl('/results-home');
  }

  onHomeClick() : void {
    this.router.navigateByUrl('/home');
  }
}
