import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-home-screen',
  templateUrl: './home-screen.component.html',
  styleUrls: ['./home-screen.component.css']
})
export class HomeScreenComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onAssessmentClick() : void {
    this.router.navigateByUrl('/assessment-intro');
  }

  onResultsClick() : void {
    this.router.navigateByUrl('/results-home');
  }
}
