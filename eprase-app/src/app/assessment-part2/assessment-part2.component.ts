import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {
  INTERVENTION_DETAILS, INTERVENTION_OVERRIDES,
  InterventionDetail,
  InterventionOverride,
  OUTCOME_RESPONSES,
  OutcomeResponse
} from "../assessment-responses";
import {Prescription, PRESCRIPTIONS} from "../prescription";
import {Patient, PATIENTS} from "../patients";
import {Assessment} from "../assessment";
import {until} from "selenium-webdriver";
import elementIsSelected = until.elementIsSelected;

@Component({
  selector: 'app-assessment-part2',
  templateUrl: './assessment-part2.component.html',
  styleUrls: ['./assessment-part2.component.css']
})
export class AssessmentPart2Component implements OnInit {

  outcomeResponses: OutcomeResponse[] = [];
  interventionDetails: InterventionDetail[] = [];
  interventionOverrides: InterventionOverride[] = [];

  prescriptions: Prescription[] = [];
  prescription: Prescription;

  patient: Patient;

  index = 0;

  constructor(private router: Router, protected assessment: Assessment) { }

  ngOnInit() {
    window.scroll(0,0);

    //BODGE for testing!
    if (!this.assessment.isIntitialised())
    {
      this.assessment.init();
    }

    this.loadResponses();
  }

  loadResponses() : void {
    this.outcomeResponses = OUTCOME_RESPONSES;
    this.interventionDetails = INTERVENTION_DETAILS;
    this.interventionOverrides = INTERVENTION_OVERRIDES;

    this.patient = PATIENTS[this.assessment.getCurrentPatientID()-1];

    for (let i = 0; i < PRESCRIPTIONS.length; i++)
    {
      if (PRESCRIPTIONS[i].patient_id == this.patient.id)
      {
        this.prescriptions.push(PRESCRIPTIONS[i])
      }
    }

    this.index = 0;
    this.prescription = this.prescriptions[this.index];
  }

  onExitClick() : void {
    this.router.navigateByUrl('/assessment-home');
  }

  onNextClick() : void {
    if (this.index<(this.prescriptions.length-1))
    {
      this.index++;
      this.prescription=this.prescriptions[this.index];

      if (this.index == (this.prescriptions.length-1))
      {
        document.getElementById("next-button").innerText = "Done";
      }
    }
    else
    {
      this.router.navigateByUrl('/assessment-home');
      this.assessment.nextPatient();
    }
  }

  onSelect1() : void {
    var selected = document.getElementById("selector-1");
    let val = selected.options[selected.selectedIndex].value;

    if (val == 0)    {                                      //unselected
      document.getElementById("next-button").disabled = true;

      document.getElementById("question-2").hidden = true;
      document.getElementById("question-3").hidden = true;
    }
    else if (val == 1) {                                    //no intervention
      document.getElementById("next-button").disabled = false;

      document.getElementById("question-2").hidden = true;
      document.getElementById("question-3").hidden = true;
    }
    else if (val == 2) {                                    //intervention
      document.getElementById("next-button").disabled = false;

      document.getElementById("question-2").hidden = false;
      document.getElementById("question-3").hidden = false;
    }
    else if (val == 3) {                                    //unable to order
      document.getElementById("next-button").disabled = false;

      document.getElementById("question-2").hidden = true;
      document.getElementById("question-3").hidden = false;
    }
  }
}
