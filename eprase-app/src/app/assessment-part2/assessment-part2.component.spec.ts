import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssessmentPart2Component } from './assessment-part2.component';

describe('AssessmentPart2Component', () => {
  let component: AssessmentPart2Component;
  let fixture: ComponentFixture<AssessmentPart2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssessmentPart2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssessmentPart2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
