import {Patient, PATIENTS} from "./patients";
import {Injectable} from "@angular/core";

@Injectable()
export class Assessment {

  private patientList: Patient[] = [];
  private initialised: boolean = false;
  private index: number = 0;
  private complete: boolean = false;

  constructor() {}

  init () : void {
    this.initPatients();
  }

  initPatients () : void {
    let ids = [];
    const num_patients = 10;

    for (let i = 0; i < num_patients; i++)
    {
      var index = Math.floor(Math.random() * PATIENTS.length);

      while (ids.includes(index))
      {
        index = Math.floor(Math.random() * PATIENTS.length);
      }

      this.patientList.push(PATIENTS[index]);
      ids.push(index);
    }

    this.initialised = true;
  }

  nextPatient() : void {
    if (this.index < 10)
    {
      this.index ++
    }
  }

  getCurrentPatient() : Patient {
    return this.patientList[this.index];
  }

  getCurrentPatientID() : number {
    return this.patientList[this.index].id;
  }

  getCurrentIndex() : number {
    return this.index;
  }

  isIntitialised(): boolean  {
    return this.initialised;
  }

  isComplete(): boolean {
    return this.complete;
  }
}

export const assessment: Assessment = new Assessment();
