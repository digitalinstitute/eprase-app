import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssessmentPart1Component } from './assessment-part1.component';

describe('AssessmentPart1Component', () => {
  let component: AssessmentPart1Component;
  let fixture: ComponentFixture<AssessmentPart1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssessmentPart1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssessmentPart1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
