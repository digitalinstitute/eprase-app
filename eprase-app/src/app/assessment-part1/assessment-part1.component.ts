import { Component, OnInit } from '@angular/core';
import { Patient, PATIENTS } from "../patients";
import { Assessment, assessment } from "../assessment";
import {Router} from "@angular/router";

@Component({
  selector: 'app-assessment-part1',
  templateUrl: './assessment-part1.component.html',
  styleUrls: ['./assessment-part1.component.css']
})
export class AssessmentPart1Component implements OnInit {

  private patient: Patient;

  constructor( private router: Router, protected assessment: Assessment ) { }

  ngOnInit() {
    //BODGE for testing!
    if (!this.assessment.isIntitialised())
    {
      this.assessment.init();
    }

    this.patient = this.assessment.getCurrentPatient();
  }

  onExitClick() : void {
    this.router.navigateByUrl('/home');
  }

  onNextClick() : void {
    this.router.navigateByUrl('/assessment-part2');
  }
}
