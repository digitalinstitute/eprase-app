import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeScreenComponent } from './home-screen/home-screen.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AboutComponent } from './about/about.component';
import { AppRoutingModule } from './/app-routing.module';
import { LoginComponent } from './login/login.component';
import { AssessmentIntroComponent } from './assessment-intro/assessment-intro.component';
import { ResultsHomeComponent } from './results-home/results-home.component';
import { AssessmentResultsComponent } from './assessment-results/assessment-results.component';
import { AssessmentHomeComponent } from './assessment-home/assessment-home.component';
import { AssessmentInstructionsComponent } from './assessment-instructions/assessment-instructions.component';
import { AssessmentPart1Component } from './assessment-part1/assessment-part1.component';
import { AssessmentPart2Component } from './assessment-part2/assessment-part2.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule } from "@angular/forms";
import { Assessment } from "./assessment";

@NgModule({
  declarations: [
    AppComponent,
    HomeScreenComponent,
    WelcomeComponent,
    AboutComponent,
    LoginComponent,
    AssessmentIntroComponent,
    ResultsHomeComponent,
    AssessmentResultsComponent,
    AssessmentHomeComponent,
    AssessmentInstructionsComponent,
    AssessmentPart1Component,
    AssessmentPart2Component,
    RegisterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [Assessment],
  bootstrap: [AppComponent]
})
export class AppModule { }
