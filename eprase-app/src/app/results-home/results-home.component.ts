import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-results-home',
  templateUrl: './results-home.component.html',
  styleUrls: ['./results-home.component.css']
})
export class ResultsHomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onBackClick() : void {
    this.router.navigateByUrl('/home');
  }

  onResultsClick() : void {
    this.router.navigateByUrl('/assessment-results');
  }
}
