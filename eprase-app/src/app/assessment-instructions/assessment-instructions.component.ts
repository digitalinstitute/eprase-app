import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-assessment-instructions',
  templateUrl: './assessment-instructions.component.html',
  styleUrls: ['./assessment-instructions.component.css']
})
export class AssessmentInstructionsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onBeginAssessmentClick() : void {
    this.router.navigateByUrl('/assessment-intro');
  }

  onHomeClick() : void {
    this.router.navigateByUrl('/home');
  }
}
