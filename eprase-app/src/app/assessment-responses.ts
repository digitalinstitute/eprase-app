export class OutcomeResponse {
  id: number;
  display: string;
  score_value: number;
}

export class InterventionDetail {
  id: number;
  display: string;
  score_value: number;
}

export class InterventionOverride {
  id: number;
  display: string;
  score_value: number;
}

//-------------------------------------------------------------

export const OUTCOME_RESPONSES: OutcomeResponse[] = [
  { id: 1,   display: 'No Intervention',            score_value: 0},
  { id: 2,   display: 'Intervention',               score_value: 1},
  { id: 3,   display: 'Unable to Initiate Order',   score_value: 6}
]

export const INTERVENTION_DETAILS: InterventionDetail[] = [
  { id: 1,   display: 'None',                               score_value: 0},
  { id: 2,   display: 'Advice on Contraindication',         score_value: 1},
  { id: 3,   display: 'Advice on Dose',                     score_value: 1},
  { id: 4,   display: 'Advice on Interaction',              score_value: 1},
  { id: 5,   display: 'Advice on Therapeutic Duplication',  score_value: 1},
  { id: 6,   display: 'Advice on Results/TDM',              score_value: 1},
  { id: 7,   display: 'Advice on Route',                    score_value: 1},
  { id: 8,   display: 'Advice on Monitoring',               score_value: 1},
  { id: 9,   display: 'Advice on Age',                      score_value: 1},
  { id: 10,  display: 'Advice on Frequency',                score_value: 1},
  { id: 11,  display: 'Advice on Indication',               score_value: 1},
  { id: 12,  display: 'Advice on Formulary',                score_value: 1},
  { id: 13,  display: 'Alternative Order Suggested',        score_value: 1}
]

export const INTERVENTION_OVERRIDES: InterventionOverride[] = [
  { id: 1,   display: 'None',                                       score_value: 0},
  { id: 2,   display: 'Unable to Override',                         score_value: 3},
  { id: 3,   display: 'Override with Change to Order',              score_value: 2},
  { id: 4,   display: 'Override with reason, No Change to Order',   score_value: 1}
]
